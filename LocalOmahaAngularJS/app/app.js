'use strict';

var app = angular.module('localOmaha', ['ngResource','ngRoute','angular.filter']);


app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/local/:businessId', {
                templateUrl: 'localOmaha/local.omaha.compass.html',
                controller: 'localOmahaCompass',
                resolve: {
                    /*business: function($route, localOmahaResource) {
                        return localOmahaResource.getAllLocalOmahaBusinesses($route.current.pathParams.businessId).$promise;
                    }*///// meh, don't have a RESTful resource to query with ID, so just pass the list to be searched
                    list: function($route, localOmahaResource) {
                        return localOmahaResource.getAllLocalOmahaBusinesses().$promise;
                    }
                }
            }).
            otherwise({
                redirectTo: '/',
                templateUrl: 'localOmaha/local.omaha.business.list.html',
                controller: 'localOmahaBusinessList'
            });
    }]);
