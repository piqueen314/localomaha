This is in reverse chronological order.

1/17/2015
----------


Below is a very interesting exception that comes from trying to read in a json file that serialized just fine earlier today!   I'm thinking of using jq (http://stedolan.github.io/jq/) to clean up the json since jackson is having trouble deserializing it.   I'm doing this with the program SimplifyTheDataMain specifically (the exception comes from the mapper.readValue(...):

~~~~~~~~~
// http://stackoverflow.com/questions/7246157/how-to-parse-a-json-string-to-an-array-using-jackson
TypeFactory typeFactory = mapper.getTypeFactory();
List<LocalBusiness> lstLocalBusiness = mapper.readValue(new File(
 inputFileName), typeFactory.constructCollectionType(List.class,
 LocalBusiness.class));
~~~~~~~~

Here's the exception:
~~~~~~~~~~~~~~~
org.codehaus.jackson.map.JsonMappingException: No suitable constructor found for type [simple type, class com.google.maps.model.LatLng]: can not instantiate from JSON object (need to add/enable type information?)
 at [Source: src/main/resources/75withLatLong.json; line: 1, column: 976] (through reference chain: org.localomaha.LocalBusiness["geoCodingResults"]->com.google.maps.model.GeocodingResult["geometry"]->com.google.maps.model.Geometry["location"])
	at org.codehaus.jackson.map.JsonMappingException.from(JsonMappingException.java:163)
	at org.codehaus.jackson.map.deser.BeanDeserializer.deserializeFromObjectUsingNonDefault(BeanDeserializer.java:746)
	at org.codehaus.jackson.map.deser.BeanDeserializer.deserializeFromObject(BeanDeserializer.java:683)
	at org.codehaus.jackson.map.deser.BeanDeserializer.deserialize(BeanDeserializer.java:580)
	at org.codehaus.jackson.map.deser.SettableBeanProperty.deserialize(SettableBeanProperty.java:299)
	at org.codehaus.jackson.map.deser.SettableBeanProperty$FieldProperty.deserializeAndSet(SettableBeanProperty.java:579)
	at org.codehaus.jackson.map.deser.BeanDeserializer.deserializeFromObject(BeanDeserializer.java:697)
	at org.codehaus.jackson.map.deser.BeanDeserializer.deserialize(BeanDeserializer.java:580)
	at org.codehaus.jackson.map.deser.SettableBeanProperty.deserialize(SettableBeanProperty.java:299)
	at org.codehaus.jackson.map.deser.SettableBeanProperty$FieldProperty.deserializeAndSet(SettableBeanProperty.java:579)
	at org.codehaus.jackson.map.deser.BeanDeserializer.deserializeFromObject(BeanDeserializer.java:697)
	at org.codehaus.jackson.map.deser.BeanDeserializer.deserialize(BeanDeserializer.java:580)
	at org.codehaus.jackson.map.deser.std.ObjectArrayDeserializer.deserialize(ObjectArrayDeserializer.java:104)
	at org.codehaus.jackson.map.deser.std.ObjectArrayDeserializer.deserialize(ObjectArrayDeserializer.java:18)
	at org.codehaus.jackson.map.deser.SettableBeanProperty.deserialize(SettableBeanProperty.java:299)
	at org.codehaus.jackson.map.deser.SettableBeanProperty$MethodProperty.deserializeAndSet(SettableBeanProperty.java:414)
	at org.codehaus.jackson.map.deser.BeanDeserializer.deserializeFromObject(BeanDeserializer.java:697)
	at org.codehaus.jackson.map.deser.BeanDeserializer.deserialize(BeanDeserializer.java:580)
	at org.codehaus.jackson.map.deser.std.CollectionDeserializer.deserialize(CollectionDeserializer.java:217)
	at org.codehaus.jackson.map.deser.std.CollectionDeserializer.deserialize(CollectionDeserializer.java:194)
	at org.codehaus.jackson.map.deser.std.CollectionDeserializer.deserialize(CollectionDeserializer.java:30)
	at org.codehaus.jackson.map.ObjectMapper._readMapAndClose(ObjectMapper.java:2732)
	at org.codehaus.jackson.map.ObjectMapper.readValue(ObjectMapper.java:1831)
	at org.localomaha.SimplifyTheDataMain.run(SimplifyTheDataMain.java:39)
	at org.localomaha.SimplifyTheDataMain.main(SimplifyTheDataMain.java:18)


~~~~~~~~~~~~~~~