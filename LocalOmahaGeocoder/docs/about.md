Uses https://developers.google.com/maps/documentation/geocoding/

Note that it says:
The Google Geocoding API has the following limits in place:

Users of the free API:
2,500 requests per 24 hour period.
5 requests per second.

https://developers.google.com/maps/documentation/webservices/client-library
https://developers.google.com/maps/documentation/webservices/client-library#gradle
https://github.com/googlemaps/google-maps-services-java
