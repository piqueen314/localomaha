package org.localomaha;


public class LocalBusinessSimple {
	private int number;
	private String address, name;
	private String category;
	private double lat;
	private double lng;

	public String toString() {
		return name + " " + address;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public void setCategory(String string) {
		this.category=string;
	}

	public void setLat(double lat) {
		this.lat=lat;
	}

	public void setLng(double lng) {
		this.lng=lng;
	}

	public String getCategory() {
		return category;
	}

	public double getLat() {
		return lat;
	}

	public double getLng() {
		return lng;
	}

}
