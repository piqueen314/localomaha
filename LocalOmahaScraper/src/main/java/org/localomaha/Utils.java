package org.localomaha;

import org.dom4j.Document;
import org.dom4j.io.DOMReader;

public class Utils {
	/**
	 * converts a W3C DOM document into a dom4j document
	 * 
	 * from http://dom4j.sourceforge.net/dom4j-1.6.1/cookbook.html
	 * 
	 * 
	 */
	public static Document buildDocment(org.w3c.dom.Document domDocument) {
		DOMReader xmlReader = new DOMReader();
		return xmlReader.read(domDocument);
	}
}
