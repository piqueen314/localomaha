LocalOmaha.org is a simple project to build developer communities while learning to use AngularJS for a mobile web app.

a. Using a smart phone, find the location of the the closest locally owned bookstore/bakery/bar?
a. Use some nifty HTML5 features -- geolocation and localstorage
a. Write an AngularJS mobile web app that interacts with webservices running on LocalOmaha.org.
a. Write those REST services with Spring using Java (and maybe a little groovy).
a. Experiment and learn about community building using a series of Coding Dojos.  "A Coding Dojo is a meeting where a bunch of coders get together to work on a programming challenge. They are there have fun and to engage in DeliberatePractice in order to improve their skills." -- http://codingdojo.org   

The adventure in community building is, perhaps, more interesting than the practice with HTML5, GeoLocation, offline storage, AngularJS, REST, Spring, and java.  Perhaps much more interesting.   Come to this talk if you want to walk through a real life use case of these technologies while hearing about how the Omaha user group community reacted to a series of Coding Dojos.

The plan is to  use a model used at the DSMWebGeeks.com:

1. Show some working code
1. State one or two challenges
1. Have folks work in small groups for a fixed amount of time
1. Have folks explain their solution and the challenges the encountered
1. Accept some merge requests
1. Repeat




